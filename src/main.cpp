#include <iostream>
#include "pond.h"
#include "io.h"


size_t number_of_solutions = 0;


// Returns true if we should break
bool checkJump(Pond* p, size_t l, size_t c, bool* frogCond, bool* emptyCond){
  char val = p->getGrid()[l*p->get_c() + c];
    
  // searching for a frog to jump over
  if(!*frogCond){
    if(val == FROG){
      *frogCond = true;
      p->delFrog(l, c);
    }
    else if(val == NOFROG){
      return true;
    }
  }

  // searching for a lily pad to land on
  else{
    if(val == NOFROG){
      *emptyCond = true;
      p->addFrog(l, c);
      return true;
    }
    if(val == FROG){
      return true;
    }
  }

  return false;
}


Pond* canJumpN(Pond* p, size_t l, size_t c){
  bool frogCond = false;
  bool emptyCond = false;
  int l_int = l;
  
  // create the new pond after the jump
  Pond* res = new Pond(*p);
  res->delFrog(l, c);

  while(--l_int >= 0){
    if(checkJump(res, l_int, c, &frogCond, &emptyCond)) break;
  }
  
  if(!frogCond || !emptyCond){
    delete res;
    res = nullptr;
  }

  return res;
}


Pond* canJumpS(Pond* p, size_t l, size_t c){
  bool frogCond = false;
  bool emptyCond = false;
  size_t max_l = p->get_l();
  
  // create the new pond after the jump
  Pond* res = new Pond(*p);
  res->delFrog(l, c);

  while(++l < max_l){
    if(checkJump(res, l, c, &frogCond, &emptyCond)) break;
  }
  
  if(!frogCond || !emptyCond){
    delete res;
    res = nullptr;
  }

  return res;
}


Pond* canJumpE(Pond* p, size_t l, size_t c){
  bool frogCond = false;
  bool emptyCond = false;
  size_t max_c = p->get_c();
  
  // create the new pond after the jump
  Pond* res = new Pond(*p);
  res->delFrog(l, c);

  while(++c < max_c){
    if(checkJump(res, l, c, &frogCond, &emptyCond)) break;
  }
  
  if(!frogCond || !emptyCond){
    delete res;
    res = nullptr;
  }

  return res;
}


Pond* canJumpW(Pond* p, size_t l, size_t c){
  bool frogCond = false;
  bool emptyCond = false;
  int c_int = c;
  
  // create the new pond after the jump
  Pond* res = new Pond(*p);
  res->delFrog(l, c);

  while(--c_int >= 0){
    if(checkJump(res, l, c_int, &frogCond, &emptyCond)) break;
  }
  
  if(!frogCond || !emptyCond){
    delete res;
    res = nullptr;
  }

  return res;
}


Pond* canJumpNE(Pond* p, size_t l, size_t c){
  bool frogCond = false;
  bool emptyCond = false;
  int l_int = l;
  size_t max_c = p->get_c();
  
  // create the new pond after the jump
  Pond* res = new Pond(*p);
  res->delFrog(l, c);

  while(--l_int >= 0 && ++c < max_c){
    if(checkJump(res, l_int, c, &frogCond, &emptyCond)) break;
  }
  
  if(!frogCond || !emptyCond){
    delete res;
    res = nullptr;
  }

  return res;
}


Pond* canJumpSE(Pond* p, size_t l, size_t c){
  bool frogCond = false;
  bool emptyCond = false;
  size_t max_l = p->get_l();
  size_t max_c = p->get_c();
  
  // create the new pond after the jump
  Pond* res = new Pond(*p);
  res->delFrog(l, c);

  while(++l < max_l && ++c < max_c){
    if(checkJump(res, l, c, &frogCond, &emptyCond)) break;
  }
  
  if(!frogCond || !emptyCond){
    delete res;
    res = nullptr;
  }

  return res;
}


Pond* canJumpSW(Pond* p, size_t l, size_t c){
  bool frogCond = false;
  bool emptyCond = false;
  int c_int = c;
  size_t max_l = p->get_l();
  
  // create the new pond after the jump
  Pond* res = new Pond(*p);
  res->delFrog(l, c);

  while(++l < max_l && --c_int >= 0){
    if(checkJump(res, l, c_int, &frogCond, &emptyCond)) break;
  }
  
  if(!frogCond || !emptyCond){
    delete res;
    res = nullptr;
  }

  return res;
}


Pond* canJumpNW(Pond* p, size_t l, size_t c){
  bool frogCond = false;
  bool emptyCond = false;
  int l_int = l;
  int c_int = c;
  
  // create the new pond after the jump
  Pond* res = new Pond(*p);
  res->delFrog(l, c);

  while(--l_int >= 0 && --c_int >= 0){
    if(checkJump(res, l_int, c_int, &frogCond, &emptyCond)) break;
  }
  
  if(!frogCond || !emptyCond){
    delete res;
    res = nullptr;
  }

  return res;
}


int solve(Pond* p, std::string s){
  size_t nfrogs = p->getNbFrogs();
  size_t nlines = p->get_l();
  size_t ncols = p->get_c();
  char* grid = p->getGrid();
  Pond* p2;
  int res = 0;

  // win condition
  if(nfrogs < 2){
    std::cout << s << std::endl;
    number_of_solutions++;
    return 1;
  }
  
  // browse each cell
  for(size_t i=0; i<nlines; ++i){
    for(size_t j=0; j<ncols; ++j){
      // test each frogs
      if(grid[i*ncols + j] == FROG){
        if((p2 = canJumpN(p, i, j)) != nullptr){
          res |= solve(p2, s + "(" + std::to_string(i) + "," + std::to_string(j) + "):N ");
          delete p2;
        }
        if((p2 = canJumpNE(p, i, j)) != nullptr){
          res |= solve(p2, s + "(" + std::to_string(i) + "," + std::to_string(j) + "):NE ");
          delete p2;
        }
        if((p2 = canJumpE(p, i, j)) != nullptr){
          res |= solve(p2, s + "(" + std::to_string(i) + "," + std::to_string(j) + "):E ");
          delete p2;
        }
        if((p2 = canJumpSE(p, i, j)) != nullptr){
          res |= solve(p2, s + "(" + std::to_string(i) + "," + std::to_string(j) + "):SE ");
          delete p2;
        }
        if((p2 = canJumpS(p, i, j)) != nullptr){
          res |= solve(p2, s + "(" + std::to_string(i) + "," + std::to_string(j) + "):S ");
          delete p2;
        }
        if((p2 = canJumpSW(p, i, j)) != nullptr){
          res |= solve(p2, s + "(" + std::to_string(i) + "," + std::to_string(j) + "):SW ");
          delete p2;
        }
        if((p2 = canJumpW(p, i, j)) != nullptr){
          res |= solve(p2, s + "(" + std::to_string(i) + "," + std::to_string(j) + "):W ");
          delete p2;
        }
        if((p2 = canJumpNW(p, i, j)) != nullptr){
          res |= solve(p2, s + "(" + std::to_string(i) + "," + std::to_string(j) + "):NW ");
          delete p2;
        }
      }
    }
  }

  return res;
}


int main(int argc, char** argv){
  if(argc != 2){
    std::cerr << "usage: " << argv[0] << " <filename>" << std::endl;
    return EXIT_FAILURE;
  }

  Pond *p = read_grid(std::string(argv[1]));

  p->printGrid();

  if(!solve(p, "")){
    std::cout << "There is no solution for this configuration" << std::endl;
  }
  else{
    std::cout << "Number of solutions: " << number_of_solutions << std::endl;
  }

  delete p;

  return EXIT_SUCCESS;
}